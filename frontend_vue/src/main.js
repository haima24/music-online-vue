import Vue from 'vue'
import App from './App.vue'
import VueRouter from 'vue-router'
import HomePage from './components/HomePage.vue'
import Playlist from './components/Playlist.vue'
import Albums from './components/Albums.vue'
import Favorite from './components/Favorite.vue'

Vue.config.productionTip = false
Vue.prototype.$api = 'https://localhost:5001'

const routes = [
  { path: '/', component: HomePage },
  { path: '/Playlist/:id', component: Playlist },
  { path: '/albums', component: Albums },
  { path: '/favorite', component: Favorite }
]

Vue.use(VueRouter);

const router = new VueRouter({
  routes // short for `routes: routes`
});

new Vue({
  router,
  render: h => h(App),
}).$mount('#app')
