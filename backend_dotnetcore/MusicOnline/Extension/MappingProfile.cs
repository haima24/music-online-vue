﻿using AutoMapper;
using MusicOnline.Models;
using MusicOnline.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MusicOnline.Extensions
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateMap<Track, TrackViewModel>()
                .ForMember(x => x.TrackId, x => x.MapFrom(p => p.TrackId))
                .ForMember(x => x.Description, x => x.MapFrom(p => p.Description))
                .ForMember(x => x.Url, x => x.MapFrom(p => p.Url));
            CreateMap<Playlist, PlayListViewModel>()
                .ForMember(x => x.PlaylistId, x => x.MapFrom(p => p.PlaylistId))
                .ForMember(x => x.Description, x => x.MapFrom(p => p.Description))
                .ForMember(x => x.BannerUrl, x => x.MapFrom(p => p.BannerUrl))
                .ForMember(x => x.ManyTrackLists, x => x.MapFrom(p => p.ManyTrackLists.Select(o => o.Track)));

            CreateMap<ManyUserFavoriteList, ManyUserFavoriteListModel>()
                .ForMember(x => x.ManyUserFavoriteListId, x => x.MapFrom(p => p.ManyUserFavoriteListId))
                .ForMember(x => x.Email, x => x.MapFrom(p => p.User.Email))
                .ForMember(x => x.UserName, x => x.MapFrom(p => p.User.FullName ?? p.User.Email))
                .ForMember(x => x.UserId, x => x.MapFrom(p => p.User.Id))
                .ForMember(x => x.PlayListId, x => x.MapFrom(p => p.PlayListId))
                .ForMember(x => x.Playlist, x => x.MapFrom(p => p.Playlist));
            CreateMap<Comment, CommentViewModel>()
               .ForMember(x => x.CommentId, x => x.MapFrom(p => p.CommentId))
               .ForMember(x => x.Email, x => x.MapFrom(p => p.User.Email))
               .ForMember(x => x.UserName, x => x.MapFrom(p => p.User.FullName ?? p.User.Email))
               .ForMember(x => x.Description, x => x.MapFrom(p => p.Description))
               .ForMember(x => x.PlaylistId, x => x.MapFrom(p => p.ParentPlayList.PlaylistId));
        }
    }
}
