﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace MusicOnline.Models
{
    public class ManyTrackArtist
    {
        [Key]
        public int ManyTrackArtistId { get; set; }

        public int TrackId { get; set; }
        public Track Track { get; set; }

        public int ArtistId { get; set; }
        public Artist Artist { get; set; }

    }
}
