﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace MusicOnline.Models
{
    public class ManyUserFavoriteList
    {
        [Key]
        public int ManyUserFavoriteListId { get; set; }

        public ApplicationUser User { get; set; }

        public int PlayListId { get; set; }
        public Playlist Playlist { get; set; }
    }
}
