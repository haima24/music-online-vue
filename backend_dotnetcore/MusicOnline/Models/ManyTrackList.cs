﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace MusicOnline.Models
{
    public class ManyTrackList
    {
        [Key]
        public int ManyTrackListId { get; set; }

        public int TrackId { get; set; }
        public Track Track { get; set; }

        public int PlayListId { get; set; }
        public Playlist Playlist { get; set; }

    }
}
