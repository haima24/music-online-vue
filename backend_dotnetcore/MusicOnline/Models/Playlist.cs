﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace MusicOnline.Models
{
    public class Playlist
    {
        [Key]
        public int PlaylistId { get; set; }

        [Required]
        [Display(Name = "Description")]
        public string Description { get; set; }


        public ICollection<ManyTrackList> ManyTrackLists { get; } = new List<ManyTrackList>();

        public string BannerUrl { get; set; }
    }
}
