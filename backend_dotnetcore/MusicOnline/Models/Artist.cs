﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace MusicOnline.Models
{
    public class Artist
    {
        [Key]
        public int ArtistId { get; set; }

        [Required]
        [Display(Name = "Description")]
        public string ArtistName { get; set; }


        public ICollection<ManyTrackArtist> ManyTrackArtists { get; } = new List<ManyTrackArtist>();
    }
}
