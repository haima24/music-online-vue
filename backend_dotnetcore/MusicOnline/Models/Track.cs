﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace MusicOnline.Models
{
    public class Track
    {
        [Key]
        public int TrackId { get; set; }

        [Required]
        [Display(Name = "Description")]
        public string Description { get; set; }

        [Required]
        public string Url { get; set; }

        public ICollection<ManyTrackList> ManyTrackLists { get; } = new List<ManyTrackList>();

        public ICollection<ManyTrackArtist> ManyTrackArtists { get; } = new List<ManyTrackArtist>();
    }
}
