﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace MusicOnline.Models
{
    public class Comment
    {
        [Key]
        public int CommentId { get; set; }

        [Required]
        [Display(Name = "Description")]
        public string Description { get; set; }

        public ApplicationUser User { get; set; }

        public Playlist ParentPlayList { get; set; }

        public string Email { get; set; }

        public string UserName { get; set; }

    }
}
