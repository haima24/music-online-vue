﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using MusicOnline.Data;
using MusicOnline.Models;
using MusicOnline.Services;

namespace MusicOnline.Services
{
    public class PlaylistService : IPlaylistService
    {

        private readonly ApplicationDbContext _dbContext;
        private readonly UserExtendManager _userExtendManager;
        public PlaylistService(ApplicationDbContext dbContext, UserExtendManager userExtendManager)
        {
            _dbContext = dbContext;
            _userExtendManager = userExtendManager;
        }
        public async Task<List<Track>> GetHomeTrackList(int takeAmount)
        {
            var trackList = new List<Track>();
            var playlist =await _dbContext.Playlists.Include(x => x.ManyTrackLists).ThenInclude(z=>z.Track).AsNoTracking()
                .FirstOrDefaultAsync(x => x.Description == "HomePageList");
            if (playlist != null) {
                trackList=playlist.ManyTrackLists.Select(x => x.Track).Take(takeAmount).ToList();
            }
            return trackList;
        }

        public async Task<Playlist> GetPlayListById(int playListId)
        {
            var trackList = new List<Track>();
            var playlist = await _dbContext.Playlists.Include(x => x.ManyTrackLists).ThenInclude(z => z.Track).AsNoTracking()
                .FirstOrDefaultAsync(x => x.PlaylistId == playListId);
            
            return playlist;
        }

        public async Task<List<Playlist>> GetAllPlayListByLatest()
        {
            var playlists = await _dbContext.Playlists.Where(x=>x.Description!= "HomePageList").OrderByDescending(x => x.PlaylistId).ToListAsync();
            return playlists;
        }

        public async Task<List<Playlist>> GetHomePagePlaylistBanners(int takeAmount)
        {
            var playlists = await _dbContext.Playlists.Where(x => x.Description != "HomePageList")
                .Take(takeAmount).OrderByDescending(x => x.PlaylistId).ToListAsync();
            return playlists;
        }

        public async Task<string> ToggleToMyFavorite(string userId, int playlistId)
        {
            var user = await _userExtendManager.FindByIdAsync(userId);
            var playList = await _dbContext.ManyUserFavoriteLists.FirstOrDefaultAsync(x => x.User.Id == userId && x.PlayListId == playlistId);
            if (playList == null)
            {
                var manyFavoriteList = new ManyUserFavoriteList { User = user, PlayListId = playlistId };
                await _dbContext.ManyUserFavoriteLists.AddAsync(manyFavoriteList);
                await _dbContext.SaveChangesAsync();
                return "Added in my favorite";
            }
            else {
                _dbContext.ManyUserFavoriteLists.Remove(playList);
                await _dbContext.SaveChangesAsync();
                return "Add to my favorite";
            }
        }

        public async Task<string> CheckPlayListFavoriteStatus(string userId, int playlistId)
        {
            var user = await _userExtendManager.FindByIdAsync(userId);
            var playList = await _dbContext.ManyUserFavoriteLists.FirstOrDefaultAsync(x => x.User.Id == userId && x.PlayListId == playlistId);
            if (playList == null)
            {
                return "Add to my favorite";
            }
            else
            {
                return "Added in my favorite";
            }
        }

        public async Task<List<Playlist>> GetAllFavoritePlaylist(string userId)
        {
            var user = await _userExtendManager.FindByIdAsync(userId);
            var playList = await _dbContext.ManyUserFavoriteLists.Where(x => x.User.Id == userId)
                .Include(x=>x.Playlist)
                .OrderByDescending(x=>x.ManyUserFavoriteListId).Select(x=>x.Playlist).ToListAsync();
            return playList;
        }

        public async Task<List<Comment>> PostPlaylistComment(int playListId,string userId,string comment)
        {
            var user = await _userExtendManager.FindByIdAsync(userId);
            var playList = await _dbContext.Playlists.FirstOrDefaultAsync(x => x.PlaylistId==playListId);
            var allComments = new List<Comment>();
            if (playList != null&&user!=null) {
                var newComment = new Comment { Description = comment, Email = user.Email, User = user, ParentPlayList = playList,
                UserName=user.UserName};
                await _dbContext.Comments.AddAsync(newComment);
                await _dbContext.SaveChangesAsync();
                allComments = await _dbContext.Comments.Include(x => x.ParentPlayList).Where(x => x.ParentPlayList.PlaylistId == playListId)
                    .ToListAsync();
            }

            return allComments;
        }

        public async Task<List<Comment>> GetCommentsByPlayListId(int playListId)
        {
            var playList = await _dbContext.Playlists.FirstOrDefaultAsync(x => x.PlaylistId == playListId);
            var allComments = new List<Comment>();
            if (playList != null)
            {
                allComments = await _dbContext.Comments.Include(x => x.ParentPlayList).Where(x => x.ParentPlayList.PlaylistId == playListId)
                    .ToListAsync();
            }
            return allComments;
        }

    }
}
