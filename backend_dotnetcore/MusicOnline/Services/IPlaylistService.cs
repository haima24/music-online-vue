﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using MusicOnline.Models;

namespace MusicOnline.Services
{
    public interface IPlaylistService
    {
        Task<List<Track>> GetHomeTrackList(int takeAmount);
        Task<Playlist> GetPlayListById(int playlistId);

        Task<List<Playlist>> GetAllPlayListByLatest();

        Task<List<Playlist>> GetHomePagePlaylistBanners(int takeAmount);

        Task<string> ToggleToMyFavorite(string userId, int playlistId);

        Task<string> CheckPlayListFavoriteStatus(string userId, int playlistId);

        Task<List<Playlist>> GetAllFavoritePlaylist(string userId);

        Task<List<Comment>> PostPlaylistComment(int playListId, string userId, string comment);

        Task<List<Comment>> GetCommentsByPlayListId(int playListId);
    }
}
