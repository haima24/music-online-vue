﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace MusicOnline.Migrations
{
    public partial class UserFavoriteListAndComment : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Comments",
                columns: table => new
                {
                    CommentId = table.Column<int>(nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    Description = table.Column<string>(nullable: false),
                    UserId = table.Column<string>(nullable: true),
                    ParentPlayListPlaylistId = table.Column<int>(nullable: true),
                    Email = table.Column<string>(nullable: true),
                    UserName = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Comments", x => x.CommentId);
                    table.ForeignKey(
                        name: "FK_Comments_Playlists_ParentPlayListPlaylistId",
                        column: x => x.ParentPlayListPlaylistId,
                        principalTable: "Playlists",
                        principalColumn: "PlaylistId",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Comments_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "ManyUserFavoriteLists",
                columns: table => new
                {
                    ManyUserFavoriteListId = table.Column<int>(nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    UserId = table.Column<string>(nullable: true),
                    PlayListId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ManyUserFavoriteLists", x => x.ManyUserFavoriteListId);
                    table.ForeignKey(
                        name: "FK_ManyUserFavoriteLists_Playlists_PlayListId",
                        column: x => x.PlayListId,
                        principalTable: "Playlists",
                        principalColumn: "PlaylistId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ManyUserFavoriteLists_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Comments_ParentPlayListPlaylistId",
                table: "Comments",
                column: "ParentPlayListPlaylistId");

            migrationBuilder.CreateIndex(
                name: "IX_Comments_UserId",
                table: "Comments",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_ManyUserFavoriteLists_PlayListId",
                table: "ManyUserFavoriteLists",
                column: "PlayListId");

            migrationBuilder.CreateIndex(
                name: "IX_ManyUserFavoriteLists_UserId",
                table: "ManyUserFavoriteLists",
                column: "UserId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Comments");

            migrationBuilder.DropTable(
                name: "ManyUserFavoriteLists");
        }
    }
}
