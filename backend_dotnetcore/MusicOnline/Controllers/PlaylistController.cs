﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using MusicOnline.Services;
using MusicOnline.ViewModels;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace MusicOnline.Controllers
{
    public class PlaylistController : Controller
    {
        private readonly IPlaylistService _playlistService;
        private readonly IMapper _mapper;

        public PlaylistController(IPlaylistService playlistService,IMapper mapper)
        {
            _playlistService = playlistService;
            _mapper = mapper;
        }

        public async Task<IActionResult> GetHomePlaylist()
        {
            var tracks =await _playlistService.GetHomeTrackList(5);
            var tracksModel = _mapper.Map<List<TrackViewModel>>(tracks);
            return Ok(tracksModel);
        }

        public async Task<IActionResult> GetPlayListById(int id,string userid)
        {
            var playList = await _playlistService.GetPlayListById(id);
            var playListModel = _mapper.Map<PlayListViewModel>(playList);
            playListModel.PlaylistFavoriteStatus = await _playlistService.CheckPlayListFavoriteStatus(userid, id);
            playListModel.Comments = _mapper.Map<List<CommentViewModel>>( await _playlistService.GetCommentsByPlayListId(id));
            return Ok(playListModel);
        }

        public async Task<IActionResult> GetAllPlayListByLatest()
        {
            var playlists = await _playlistService.GetAllPlayListByLatest();
            var playlistsModels = _mapper.Map<List<PlayListViewModel>>(playlists);
            return Ok(playlistsModels);
        }

        public async Task<IActionResult> HompagePlaylistBanners()
        {
            var playlists = await _playlistService.GetHomePagePlaylistBanners(3);
            var playlistsModels = _mapper.Map<List<PlayListViewModel>>(playlists);
            return Ok(playlistsModels);
        }

        [HttpPost]
        [Authorize(AuthenticationSchemes = "Bearer")]
        public async Task<IActionResult> ToggleToMyFavorite([FromBody]ManyUserFavoriteListModel model)
        {
            var user = User.FindFirst(JwtRegisteredClaimNames.Sid);
            var status = string.Empty;
            if (user != null) {
                status = await _playlistService.ToggleToMyFavorite(user.Value, model.PlayListId);
            }
            
            return Ok(status);
        }

        [HttpGet]
        [Authorize(AuthenticationSchemes = "Bearer")]
        public async Task<IActionResult> GetAllFavoritePlaylist()
        {
            var user = User.FindFirst(JwtRegisteredClaimNames.Sid);
            var playlists= await _playlistService.GetAllFavoritePlaylist(user.Value);
            var playlistsModels = _mapper.Map<List<PlayListViewModel>>(playlists);
            return Ok(playlistsModels);
        }

        [HttpPost]
        [Authorize(AuthenticationSchemes = "Bearer")]
        public async Task<IActionResult> PostPlaylistComment([FromBody]CommentViewModel comment)
        {
            var user = User.FindFirst(JwtRegisteredClaimNames.Sid);
            var commentsModel = new List<CommentViewModel>();
            if (user != null)
            {
                var comments = await _playlistService.PostPlaylistComment(comment.PlaylistId, user.Value, comment.Description);
                commentsModel = _mapper.Map<List<CommentViewModel>>(comments);
            }

            return Ok(commentsModel);
        }
    }
}
