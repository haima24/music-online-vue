﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MusicOnline.ViewModels
{
    public class PlayListViewModel
    {
        public int PlaylistId { get; set; }

        public string Description { get; set; }


        public List<TrackViewModel> ManyTrackLists { get; set; }

        public string BannerUrl { get; set; }

        public string PlaylistFavoriteStatus { get; set; }

        public List<CommentViewModel> Comments { get; set; }
    }
}
