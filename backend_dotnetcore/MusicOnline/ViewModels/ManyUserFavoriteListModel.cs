﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MusicOnline.ViewModels
{
    public class ManyUserFavoriteListModel
    {
        public int ManyUserFavoriteListId { get; set; }

        public string UserName { get; set; }

        public string Email { get; set; }

        public string UserId { get; set; }

        public int PlayListId { get; set; }
        public PlayListViewModel Playlist { get; set; }

    }
}
