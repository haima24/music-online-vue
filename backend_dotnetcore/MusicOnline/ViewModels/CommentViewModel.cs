﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace MusicOnline.ViewModels
{
    public class CommentViewModel
    {
        public int CommentId { get; set; }

        public string Description { get; set; }

        public int PlaylistId { get; set; }

        public string Email { get; set; }

        public string UserName { get; set; }
    }
}
