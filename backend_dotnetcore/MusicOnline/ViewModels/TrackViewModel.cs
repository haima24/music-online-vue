﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MusicOnline.ViewModels
{
    public class TrackViewModel
    {
        public int TrackId { get; set; }

        public string Description { get; set; }

        public string Url { get; set; }

    }
}
