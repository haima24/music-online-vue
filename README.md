# Music Online - VueJS + Dotnet Core 2.1 - Tu Huynh

## Demo Urls

# This web application using techniques:

##	Backend: ASP.NET Core MVC 2.1 ( ex: https://localhost:5001 ) 
##	VueJS ( SPA ) for building user interfaces ( ex: localhost:8080 )

# This web application can be run by :

##	 download and install .NET Core from here https://dotnet.microsoft.com/download/dotnet-core/2.1

##	From .NET Core Project folder that contain .csproj file (/backend_dotnetcore/MusicOnline), open cmd and type **dotnet run**( server side)

##	This web-app using JWT for Authentication and Authorization
 

###	In project that contain VueJS source (/frontend_vue), open cmd and type **npm run serve** ( SPA )
###	For database, using SQL Lite ( MusicOnline.db ) file for this website,
###	All source codes are in gitlab ( public ): https://gitlab.com/haima24/music-online-vue

APIs:

- Register: https://localhost:5001/Account/Register

- Login: https://localhost:5001/Account/Login

- View PlayList: https://localhost:5001/playlist/GetPlayListById/3?userid=e4c93955-5b98-4adf-9a33-e298380cae35

- View Albums: https://localhost:5001/playlist/GetAllPlayListByLatest

- View Favorite PlayList: https://localhost:5001/playlist/GetAllFavoritePlaylist

- Comment: https://localhost:5001/playlist/PostPlaylistComment

- Add or remove playlist to favorite albums: https://localhost:5001/playlist/ToggleToMyFavorite
